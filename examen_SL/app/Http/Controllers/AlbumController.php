<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use examen_sl\Album;


class AlbumController extends Controller
{
    //
     public function index ()
    {
    	//
    	$albums = Album::get();
  
        return view('albums.index', compact('albums' $albums));
    }
      public function create ()
    {
    	//
    	return view('albums.create');

    }

       public function store (Request $request, $artista_id)
    {
    	//
    	$album = new Album;
    	$album->nombre = $request->input('nombre');
    	$album->artista_id -> input('artista_id');

    	$album->save();

        return redirect()->route('albums.index');
    }

    public function edit ($id)
    {
    	//
    	 $artista = Album::find($id);
        return view('albums.edit')->with('albums',$artista);
    }

     public function update (Request $request, $id)
    {
    	//
    	$album = Album::find($id);
        $album->nombre = $request->input('nombre');
        $album->artista_id -> input('artista_id');
        $album->save();
        return redirect()->route('albums.index');
    }

      public function destroy ()
    {
    	//
    	Album::destroy($id);
        return redirect()->route('album.index');
    }
}
