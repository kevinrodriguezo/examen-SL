<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use examen_sl\Album_Oyente;

class Albums_OyentesController extends Controller
{
    //
     public function index ()
    {
    	//
    	$albums_oyentes = Album_Oyente::get();
  
        return view('albums_oyentes.index', compact('albums_oyentes' $albums_oyentes));
    }
      public function create ()
    {
    	//
    	return view('albums_oyentes.create');

    }

       public function store (Request $request, $artista_id)
    {
    	//
    	$album_oyente = new Album_Oyente;
    	$album_oyente->album_id = $request->input('album_id');
    	$album_oyente->oyente_id =$request-> input('oyente_id');

    	$album_oyente->save();

        return redirect()->route('albums_oyentes.index');
    }

    public function edit ($id)
    {
    	//
    	 $artista = Album_Oyente::find($id);
        return view('albums_oyentes.edit')->with('albums_oyentes',$albums_oyentes);
    }

     public function update (Request $request, $id)
    {
    	//
    	$album_oyente = Album_Oyente::find($id);
    	$album_oyente->album_id = $request->input('album_id');
    	$album_oyente->oyente_id =$request-> input('oyente_id');
        $album_oyente->save();
        return redirect()->route('albums_oyentes.index');
    }

      public function destroy ()
    {
    	//
    	Album::destroy($id);
        return redirect()->route('albums_oyentes.index');
    }
}
