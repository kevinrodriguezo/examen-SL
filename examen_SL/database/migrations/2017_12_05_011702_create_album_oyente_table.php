<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAlbumOyenteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('album_oyente', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('album_id')->unsigned();
            $table->foreign('album_id')->references('id')->on('albums');


            $table->integer('oyente_id')->unsigned();
            $table->foreign('oyente_id')->references('id')->on('oyentes');


            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('album_oyente');
    }
}
