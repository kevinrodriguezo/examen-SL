<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;

class AlbumSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
         $faker = Faker::create();
		for ($i=0; $i < 10; $i++) {
    	\DB::table('albums')->insert(array(
           'nombre' => $faker->randomElement(['CLASICOS','NUEVA VIDA','ALBUM EXPLOSIVO']),
           'artista_id' => '1',
           'created_at' => date('Y-m-d H:m:s'),
           'updated_at' => date('Y-m-d H:m:s')
    ));
}
    }
}
