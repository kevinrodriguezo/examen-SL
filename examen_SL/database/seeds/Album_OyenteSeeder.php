<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;


class Album_OyenteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
              $faker = Faker::create();
		for ($i=0; $i < 10; $i++) {
    	\DB::table('album_oyente')->insert(array(
           'album_id' => '1',
           'oyente_id' => '1',
           'created_at' => date('Y-m-d H:m:s'),
           'updated_at' => date('Y-m-d H:m:s')
    ));
}
    }
}
